﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs.Specialized;
using ServerSideBlobTransfer;
using System.Text;

namespace NonConcatChunked;

public class Program
{
    private const string _connectionString = "DefaultEndpointsProtocol=https;AccountName=mzatloukallearning;AccountKey=73+Ehj0l7z0oCEiDhwF2z4tYFKh9fabvLFceOuOeXLFbfasxfFIkdkDENtQJZ/L+wMGnaKNwN4+hxqhQqNrVxw==;EndpointSuffix=core.windows.net";
    public static async Task Main(string[] args)
    {
        ServerSideBlobTransferer transferer = new ServerSideBlobTransferer(_connectionString);

        await transferer.CombineRawBlobsIntoOneConsumerBlockBlobAsync("append", "message");
    }

    public static async Task CreateAppendBlob(string blobName)
    {
        AppendBlobClient appendBlobClient = new AppendBlobClient(_connectionString, "raw", blobName);

        await appendBlobClient.CreateIfNotExistsAsync();

        int blockNumber = 0;

        while (blockNumber < 10)
        {
            blockNumber++;
            byte[] blockContent = Encoding.UTF8.GetBytes(blockNumber.ToString());
            using (var ms = new MemoryStream(blockContent))
            {
                await appendBlobClient.AppendBlockAsync(ms);
            }
        }
    }

    public static async Task CreateBlockBlob(string blobName)
    {
        BlockBlobClient blockBlobClient = new BlockBlobClient(_connectionString, "raw", blobName);

        List<string> blocks = new List<string>();
        int blockNumber = 0;

        while (blockNumber < 2)
        {
            blockNumber++;
            var blockId = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

            byte[] blockContent = Enumerable.Repeat(blockNumber, 1000_000_000).ToArray().Select(n => Convert.ToByte(n)).ToArray();
            using (var ms = new MemoryStream(blockContent))
            {
                await blockBlobClient.StageBlockAsync(blockId, ms);
            }
            blocks.Add(blockId);
        }

        await blockBlobClient.CommitBlockListAsync(blocks);
    }
}
