﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs.Specialized;
using Azure.Storage.Sas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideBlobTransfer;

public class ServerSideBlobTransferer
{
    // 4 000 MiB = 4 000 0000 000 B
    private long _maximumBlockSize = 4_000_000_000;
    private int _maximumNumberOfBlocks = 50_000;

    private readonly BlobContainerClient _rawZoneContainerClient;
    private readonly BlobContainerClient _consumerZoneContainerClient;

    public ServerSideBlobTransferer(string storageAccountConnectionString)
    {
        var blobServiceClient = new BlobServiceClient(storageAccountConnectionString);
        _rawZoneContainerClient = blobServiceClient.GetBlobContainerClient("raw");
        _consumerZoneContainerClient = blobServiceClient.GetBlobContainerClient("consumer");
    }

    /// <summary>
    /// Copies multiple blobs of any type (e.g. append/block) from raw zone and combines them into one block blob in consumer zone. Lexicographical order of source blobs specifies the resulting order in the desination blob.
    /// This operation is idempotent - e.i. if the destination blob already exists, it's content will be rewritten.
    /// </summary>
    public async Task CombineRawBlobsIntoOneConsumerBlockBlobAsync(string sourceBlobPrefix, string destinationBlobName)
    {
        var destinationBlobClient = _consumerZoneContainerClient.GetBlockBlobClient(destinationBlobName);
        List<string> destinationBlocks = new List<string>();

        var sourceBlobsRequest = _rawZoneContainerClient.GetBlobsAsync(prefix: sourceBlobPrefix);

        List<BlobItem> sourceBlobs = new List<BlobItem>();

        await foreach(var sourceBlob in sourceBlobsRequest)
        {
            sourceBlobs.Add(sourceBlob);
        }

        if (sourceBlobs.Count > _maximumNumberOfBlocks)
        {
            throw new InvalidOperationException("The input would exeeded 50 000 of blocks in the destination blob.");
        }

        foreach (var sourceBlob in sourceBlobs)
        {
            BlobSasBuilder sasBuilder = new BlobSasBuilder()
            {
                BlobContainerName = "raw",
                BlobName = sourceBlob.Name,
                ExpiresOn = DateTimeOffset.UtcNow.AddDays(1)
            };

            sasBuilder.SetPermissions(BlobAccountSasPermissions.Read);

            var sourceBlobClient = _rawZoneContainerClient.GetBlobClient(sourceBlob.Name);
            var sourceBlobLength = sourceBlob.Properties.ContentLength;

            if (sourceBlobLength is null)
            {
                continue;
            }

            var blocksNeeded = Math.Round((decimal)sourceBlobLength / _maximumBlockSize, MidpointRounding.ToPositiveInfinity);

            if (blocksNeeded > _maximumNumberOfBlocks)
            {
                throw new InvalidOperationException("The input would exeeded 50 000 of blocks in the destination blob.");
            }

            var sourceBlobSasUri = sourceBlobClient.GenerateSasUri(sasBuilder);
            int sourceBlockCount = 0;

            while (sourceBlockCount * _maximumBlockSize < sourceBlobLength)
            {
                var blockId = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

                long? readLength = null;

                if ((sourceBlockCount + 1) * _maximumBlockSize < sourceBlobLength)
                {
                    readLength = _maximumBlockSize;
                }

                await destinationBlobClient.StageBlockFromUriAsync(sourceBlobSasUri, blockId, new StageBlockFromUriOptions
                {
                    SourceRange = new HttpRange(sourceBlockCount * _maximumBlockSize, readLength)
                });

                destinationBlocks.Add(blockId);
                ++sourceBlockCount;
            }
        }

        await destinationBlobClient.CommitBlockListAsync(destinationBlocks);
    }
}
